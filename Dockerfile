FROM ubuntu:20.04

ENV LANG C.UTF-8
RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN apt-get update
RUN apt-get install -y nocache python3.9 python3-pip \
    python3.9-venv curl python3.9-dev python3.9-venv git vim
    
# делаем ссылку, что команда python это python3
RUN [ -f /usr/bin/python ] || ln -s /usr/bin/python3.9 /usr/bin/python

# Устанавливаем poetry
RUN [ -f $HOME/.local/bin/poetry ] || \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
    
RUN ln -s $HOME/.local/bin/poetry /usr/bin/poetry

# Разрешаем poetry устанавливать зависимости в .venv проекта
RUN $HOME/.local/bin/poetry config virtualenvs.in-project true

# Установка pyenv
RUN git clone https://github.com/pyenv/pyenv.git ~/.pyenv

# Настройка pyenv
RUN echo 'export PYENV_ROOT=$HOME/.pyenv' > ~/.bashrc
RUN echo 'export PATH=$PYENV_ROOT/bin:$HOME/.local/bin:$PATH' >> ~/.bashrc
RUN echo "$($HOME/.pyenv/bin/pyenv init --path)" >> ~/.bashrc
RUN echo 'export LC_TYPE="C.UTF-8"' >> ~/.bashrc

